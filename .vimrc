if v:lang =~ "utf8$" || v:lang =~ "UTF-8$"
   set fileencodings=utf-8,latin1
endif

set hidden " to move hidden buffers easily

" spelling
" toggle spellchecking on and off with `,s`
let mapleader = ","
nmap <silent> <leader>s :set spell!<CR>
" set region for dict
set spelllang=en_us

set nocompatible        " Use Vim defaults (much better!)
set scrolloff=3         " Keep 3 lines while scrolling
set bs=2                " allow backspacing over everything in insert mode
set ai
set viminfo='20,\"50    " read/write a .viminfo file, don't store more
                        " than 50 lines of registers
set history=50          " keep 50 lines of command line history
set ruler               " show the cursor position all the time
set number		" show line number
set showcmd             " Show incomplete commands
set hlsearch            " highlights search matches
set incsearch           " Do incremental searching
set smartcase           " No ignorecase is Uppercase character is present
set showmatch           " show matching braces
set showmode            " show mode that VIM is in
set backspace=indent,eol,start  " make that backspace key work the way it should
set t_Co=256
set t_RV=               " http://bugs.debian.org/608242, http://groups.google.com/group/vim_dev/browse_thread/thread/9770ea844cec3282

syntax enable
set background=dark
let g:solarized_termcolors=256
let g:solarized_contrast="high"
let g:solarized_visibility="high"
colorscheme solarized

map <silent> <leader>l :set background=light<CR>
cmap w!! % ! sudo tee 2> /dev/null %
